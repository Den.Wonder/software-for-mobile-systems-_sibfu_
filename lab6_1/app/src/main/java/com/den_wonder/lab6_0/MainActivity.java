package com.den_wonder.lab6_0;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import android.widget.SimpleAdapter;

import static com.den_wonder.lab6_0.R.id.main_text;
import static com.den_wonder.lab6_0.R.id.second_text;

public class MainActivity extends ListActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_list);

        this.customSimpleAdapterListView();
    }

    private void customSimpleAdapterListView(){
        String[] languages = {
                "C", "C++", "C#", "Perl",
                "Haskell", "LISP", "Pascal", "Java",
                "Kotlin", "JavaScript", "TypeScript", "Python",
                "Assembler", "PHP", "Go", "Ruby",
                "R"
        };
        final ArrayList<Map<String,Object>> itemDataList = new ArrayList<Map<String,Object>>();

        int titleLen = languages.length;
        for(int i =0; i < titleLen; i++) {
            Map<String,Object> listItemMap = new HashMap<String,Object>();
            listItemMap.put("imageId", R.mipmap.ic_launcher);
            listItemMap.put("main_text", languages[i]);
            listItemMap.put("second_text", languages[i]);
            itemDataList.add(listItemMap);
        }

        final SimpleAdapter simpleAdapter = new SimpleAdapter(this,itemDataList,
                R.layout.custom_list,
                new String[]{"imageId","main_text","second_text"},
                new int[]{R.id.userImage,main_text, second_text});

        final ListView listView = (ListView)findViewById(android.R.id.list);
        listView.setAdapter(simpleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                HashMap clickItemMap = (HashMap)clickItemObj;
                String itemTitle = (String)clickItemMap.get("main_text");
                String itemDescription = (String)clickItemMap.get("second_text");

                Toast.makeText(getApplicationContext(),
                "Вы выбрали язык " + itemTitle
                        + "\nВ данном списке он находится на "+(index+1)+" позиции.",
                Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                itemDataList.remove(position);
                simpleAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"you have deleted language from the list!",Toast.LENGTH_SHORT).show();

                return true;
            }
        });

    }
}