package com.den_wonder.lab8_0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public boolean isSunning = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sunDown(View view) {
        if(isSunning==true) {


            ImageView sunImageView = findViewById(R.id.sun);
            Animation sunDownAnimation = AnimationUtils.loadAnimation(this, R.anim.sun_down);
            sunImageView.startAnimation(sunDownAnimation);

            ImageView clockImageView = findViewById(R.id.clock);
            Animation clockTurnAnimation = AnimationUtils.loadAnimation(this, R.anim.clock_turn);
            clockImageView.startAnimation(clockTurnAnimation);

            ImageView hourImageView = findViewById(R.id.hour_hand);
            Animation hourTurnAnimation = AnimationUtils.loadAnimation(this, R.anim.hour_turn);
            hourImageView.startAnimation(hourTurnAnimation);

            isSunning=!isSunning;

        }
        else{
            Toast.makeText(getApplicationContext(),"Now is the night",Toast.LENGTH_SHORT).show();
        }
    }

    public void sunRise(View view) {
        if(isSunning==false) {
            ImageView sunImageView = findViewById(R.id.sun);
            Animation sunRiseAnimation = AnimationUtils.loadAnimation(this, R.anim.sun_rise);
            sunImageView.startAnimation(sunRiseAnimation);

            ImageView clockImageView = findViewById(R.id.clock);
            Animation clockTurnAnimation = AnimationUtils.loadAnimation(this, R.anim.clock_turn);
            clockImageView.startAnimation(clockTurnAnimation);

            ImageView hourImageView = findViewById(R.id.hour_hand);
            Animation hourTurnAnimation = AnimationUtils.loadAnimation(this, R.anim.hour_turn);
            hourImageView.startAnimation(hourTurnAnimation);

            isSunning = !isSunning;
        }
        else{
            Toast.makeText(getApplicationContext(),"SUN IS ALREADY RISE!", Toast.LENGTH_SHORT).show();
        }
    }
}
