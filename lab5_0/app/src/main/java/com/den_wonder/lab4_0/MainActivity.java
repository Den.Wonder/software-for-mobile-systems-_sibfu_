package com.den_wonder.lab4_0;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TableLayout tablelayout = findViewById(R.id.main_layout);

        registerForContextMenu(tablelayout);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view,R.string.firstSnackText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case(R.id.action_fastToast):{
                fastToast1();
                return true;
            }
            case (R.id.action_slowToast):{
                slowToast1();
                return true;
            }
            case(R.id.action_imgToast):{
                imgToast1();
                return true;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case(R.id.action_fastToast):{
                fastToast1();
                return true;
            }
            case (R.id.action_slowToast):{
                slowToast1();
                return true;
            }
            case(R.id.action_imgToast):{
                imgToast1();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void fastToast(View view) {
        Toast fastToastText = Toast.makeText(getApplicationContext(),
                R.string.shortToastText, Toast.LENGTH_SHORT);
        fastToastText.setGravity(Gravity.CENTER, 0, 0);
        fastToastText.show();
    }

    public void slowToast(View view) {
        Toast slowToastText = Toast.makeText(getApplicationContext(),
                R.string.longToastText, Toast.LENGTH_LONG);
        slowToastText.setGravity(Gravity.CENTER, 0, 0);
        slowToastText.show();
    }

    public void imgToast(View view){
        Toast toastWithImage = Toast.makeText(getApplicationContext(),
                R.string.imgToastText, Toast.LENGTH_LONG);
        toastWithImage.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout toastContainer = (LinearLayout) toastWithImage.getView();
        ImageView spaceImgView = new ImageView(getApplicationContext());
        spaceImgView.setImageResource(R.drawable.spaceman);
        toastContainer.addView(spaceImgView, 0);
        toastWithImage.show();
    }

    public void fastToast1() {
        Toast fastToastText = Toast.makeText(getApplicationContext(),
                R.string.shortToastText, Toast.LENGTH_SHORT);
        fastToastText.setGravity(Gravity.CENTER, 0, 0);
        fastToastText.show();
    }

    public void slowToast1() {
        Toast slowToastText = Toast.makeText(getApplicationContext(),
                R.string.longToastText, Toast.LENGTH_LONG);
        slowToastText.setGravity(Gravity.CENTER, 0, 0);
        slowToastText.show();
    }

    public void imgToast1(){
        Toast toastWithImage = Toast.makeText(getApplicationContext(),
                R.string.imgToastText, Toast.LENGTH_LONG);
        toastWithImage.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout toastContainer = (LinearLayout) toastWithImage.getView();
        ImageView spaceImgView = new ImageView(getApplicationContext());
        spaceImgView.setImageResource(R.drawable.spaceman);
        toastContainer.addView(spaceImgView, 0);
        toastWithImage.show();
    }

    public void secondSnack(View view){
        Snackbar.make(view, R.string.secondSnackText, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }





}
