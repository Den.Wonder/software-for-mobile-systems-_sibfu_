package com.den_wonder.lab2_0;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LightSide extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.light_side);

        String user = "Юный падаван";
        user=getIntent().getExtras().getString("username");
        TextView hello = (TextView)findViewById(R.id.light_textview1);
        hello.setText("Здравствуй, "+user+ "!");

    }

    public void onBackBtnClickLight(View view) {
        finish();
    }
}
