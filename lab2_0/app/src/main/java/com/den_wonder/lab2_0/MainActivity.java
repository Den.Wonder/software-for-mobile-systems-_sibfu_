package com.den_wonder.lab2_0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onLightSideBtnClick(View view) {
        Intent intent = new Intent(MainActivity.this, LightSide.class);
        EditText userEditName = (EditText) findViewById(R.id.user_name_edit);
        intent.putExtra("username", userEditName.getText().toString());
        startActivity(intent);
    }

    public void onDarkSideBtnClick(View view) {
        Intent intent2 = new Intent(MainActivity.this, DarkSide.class);
        EditText userEditName = (EditText) findViewById(R.id.user_name_edit);
        intent2.putExtra("username", userEditName.getText().toString());
        startActivity(intent2);
    }
}
