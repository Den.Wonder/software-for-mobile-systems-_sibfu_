package com.den_wonder.lab2_0;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

public class DarkSide extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dark_side);
        String user = "Юный падаван";
        user=getIntent().getExtras().getString("username");
        TextView hello = (TextView)findViewById(R.id.dark_textView);
        hello.setText("Приветствую тебя, Дарт "+user+ "!");
    }

    public void onBackBtnClickDark(View view) {
        finish();
    }
}
