package com.den_wonder.lab6_0;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends ListActivity implements AdapterView.OnItemLongClickListener {

    final String[] languagesArray= new String[]{
            "C", "C++", "C#", "Perl",
            "Haskell", "LISP", "Pascal", "Java",
            "Kotlin", "JS", "TypeScript", "Python",
            "Assembler", "php", "Go", "Ruby"
    };

    private ArrayAdapter<String> mAdapter;
    private ArrayList<String> languagesList = new ArrayList<>(Arrays.asList(languagesArray));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, languagesList);
        setListAdapter(mAdapter);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Toast.makeText(getApplicationContext(),
                "Вы выбрали язык " + l.getItemAtPosition(position).toString()
                        + "\nВ данном списке он находится на "+(position +1)+" позиции.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();

        mAdapter.remove(selectedItem);
        mAdapter.notifyDataSetChanged();

        Toast.makeText(getApplicationContext(),
                selectedItem + " Был удалён из списка.",
                Toast.LENGTH_SHORT).show();

        return true;
    }
}
