package com.den_wonder.lab7_0.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.den_wonder.lab7_0.data.HotelContract.GuestEntry;

import static com.den_wonder.lab7_0.data.HotelContract.GuestEntry.TABLE_NAME;

public class HotelDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = HotelDbHelper.class.getSimpleName();

    //database name
    private static final String DATABASE_NAME = "hotel.db";
    public static final int DATABASE_VERSION = 1;
    public HotelDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String SQL_CREATE_GUESTS_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + HotelContract.GuestEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + GuestEntry.COLUMN_NAME + " TEXT NOT NULL, "
                + GuestEntry.COLUMN_CITY + " TEXT NOT NULL, "
                + GuestEntry.COLUMN_GENDER + " INTEGER NOT NULL DEFAULT 3, "
                + GuestEntry.COLUMN_AGE + " INTEGER NOT NULL DEFAULT 0);";

        db.execSQL(SQL_CREATE_GUESTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //   Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);
  //      db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
//        onCreate(db);
    }

    public int deleteData(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "name = ?", new String[] {name});
    }

    public void deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
    }



}
