package com.den_wonder.lab7_0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.den_wonder.lab7_0.data.HotelContract;
import com.den_wonder.lab7_0.data.HotelDbHelper;
import com.den_wonder.lab7_0.data.HotelContract.GuestEntry;


public class EditorActivity extends AppCompatActivity {

    private EditText mNameEditText;
    private EditText mCityEditText;
    private EditText mAgeEditText;
    private Spinner mGenderSpinner;

    private int mGender = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        mNameEditText = findViewById(R.id.edit_guest_name);
        mCityEditText = findViewById(R.id.edit_guest_city);
        mAgeEditText = findViewById(R.id.edit_guest_age);
        mGenderSpinner = findViewById(R.id.spinner_gender);

        setupSpinner();
    }

    private void setupSpinner() {
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_dropdown_item_1line);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mGenderSpinner.setAdapter(genderSpinnerAdapter);
        mGenderSpinner.setSelection(2);

        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_female))) {
                        mGender = HotelContract.GuestEntry.GENDER_FEMALE; // female
                    } else if (selection.equals(R.string.gender_male)) {
                        mGender = HotelContract.GuestEntry.GENDER_MALE; // male
                    } else {
                        mGender = HotelContract.GuestEntry.GENDER_UNKNOWN; // Не определен
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = 2;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                insertGuest();
                // Закрываем активность
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:

                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteData(){

        HotelDbHelper mDbHelper = new HotelDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Integer deletedRows = mDbHelper.deleteData(mNameEditText.getText().toString());
    }

    private void insertGuest(){
        String name = mNameEditText.getText().toString().trim();
        String city = mCityEditText.getText().toString().trim();
        String ageString = mAgeEditText.getText().toString().trim();
        int age = Integer.parseInt(ageString);

        if((name.isEmpty())||(city.isEmpty())||(ageString.isEmpty()))
        {
            Toast.makeText(this, "Введены не все данные", Toast.LENGTH_SHORT).show();

        }

        HotelDbHelper mDbHelper = new HotelDbHelper(this);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GuestEntry.COLUMN_NAME, name);
        values.put(GuestEntry.COLUMN_CITY, city);
        values.put(GuestEntry.COLUMN_GENDER, mGender);
        values.put(GuestEntry.COLUMN_AGE, age);

        // Вставляем новый ряд в базу данных и запоминаем его идентификатор
        long newRowId = db.insert(GuestEntry.TABLE_NAME, null, values);

        // Выводим сообщение в успешном случае или при ошибке
        if (newRowId == -1) {
            // Если ID  -1, значит произошла ошибка
            Toast.makeText(this, "Ошибка при заведении гостя", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Гость заведён под номером: " + newRowId, Toast.LENGTH_SHORT).show();
        }
    }

}

