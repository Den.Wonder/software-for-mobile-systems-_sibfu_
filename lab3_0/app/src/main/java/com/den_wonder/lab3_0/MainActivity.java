package com.den_wonder.lab3_0;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{
    private TextView VerticalTV;
    private TextView HorizontalTV;
    private Button vbtn1,vbtn2,vbtn3,vbtn4,vbtn5,vbtn6,hbtn1,hbtn2,hbtn3,hbtn4,hbtn5,hbtn6;
    public boolean language = false;
    public int clr = 0;
    static final String ORIENTATION_PORTRAIT = "Портретная ориентация";
    static final String ORIENTATION_LANDSCAPE = "Альбомная ориентация";
    private static final Boolean KEY_LANG = true;
    private static final Integer KEY_COLOR = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            language = savedInstanceState.getBoolean(String.valueOf(KEY_LANG));
            clr = savedInstanceState.getInt(String.valueOf(KEY_COLOR));
        }
        themes();
        setContentView(R.layout.activity_main);


        VerticalTV = (TextView)findViewById(R.id.VTextView);
        HorizontalTV = (TextView)findViewById(R.id.HTextView);
        vbtn1 = (Button)findViewById(R.id.vbutton1);
        vbtn2 = (Button)findViewById(R.id.vbutton2);
        vbtn3 = (Button)findViewById(R.id.vbutton3);
        vbtn4 = (Button)findViewById(R.id.vbutton4);
        vbtn5 = (Button)findViewById(R.id.vbutton5);
        vbtn6 = (Button)findViewById(R.id.vbutton6);
        hbtn1 = (Button)findViewById(R.id.hbutton1);
        hbtn2 = (Button)findViewById(R.id.hbutton2);
        hbtn3 = (Button)findViewById(R.id.hbutton3);
        hbtn4 = (Button)findViewById(R.id.hbutton4);
        hbtn5 = (Button)findViewById(R.id.hbutton5);
        hbtn6 = (Button)findViewById(R.id.hbutton6);

    }


    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBoolean(String.valueOf(KEY_LANG),language);
        outState.putInt(String.valueOf(KEY_COLOR),clr);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        lang();
        themes();
    }

    public void themes() {
        switch (clr) {
            case 0: {
                setTheme(R.style.AppTheme);
                break;
            }
            case 1: {
                setTheme(R.style.RedTheme);
                break;
            }
            case 2: {
                setTheme(R.style.BlueTheme);
                break;
            }
            case 3: {
                setTheme(R.style.BlackTheme);
                break;
            }
        }
    }

    private void lang() {
        if(language){
            if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
                vbtn1.setText(R.string.btn1_text_en);
                vbtn2.setText(R.string.btn2_text_en);
                vbtn3.setText(R.string.btn3_text_en);
                vbtn4.setText(R.string.btn4_text_en);
                vbtn5.setText(R.string.btn5_text_en);
                vbtn6.setText(R.string.btn6_text_en);
            }
            else if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE) {
                 hbtn1.setText(R.string.btn1_text_en);
                 hbtn2.setText(R.string.btn2_text_en);
                 hbtn3.setText(R.string.btn3_text_en);
                 hbtn4.setText(R.string.btn4_text_en);
                 hbtn5.setText(R.string.btn5_text_en);
                 hbtn6.setText(R.string.btn6_text_en);
            }
        }
        else if (!language) {
            if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
                vbtn1.setText(R.string.btn1_text);
                vbtn2.setText(R.string.btn2_text);
                vbtn3.setText(R.string.btn3_text);
                vbtn4.setText(R.string.btn4_text);
                vbtn5.setText(R.string.btn5_text);
                vbtn6.setText(R.string.btn6_text);
            }
            else if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE) {
                 hbtn1.setText(R.string.btn1_text);
                 hbtn2.setText(R.string.btn2_text);
                 hbtn3.setText(R.string.btn3_text);
                 hbtn4.setText(R.string.btn4_text);
                 hbtn5.setText(R.string.btn5_text);
                 hbtn6.setText(R.string.btn6_text);
            }
        }
    }

    public void sayHello(View view){
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
            VerticalTV.setText(R.string.hello);
        }
        else if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            HorizontalTV.setText(R.string.hello);
        }
    }

    public void showWindow(View view){
        Intent show = new Intent(MainActivity.this, WindowActivity.class);
        startActivity(show);
    }

    public void getOrient(View view) {
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
            VerticalTV.setText(R.string.vertical);
        }
        else if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            HorizontalTV.setText(R.string.horizontal);
        }
    }

    public void langChange(View view){
        language = !language;
        lang();
    }

    public void colorChange(View view){
        clr++;
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
            VerticalTV.setText("Theme # "+clr);
        }
        else if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            HorizontalTV.setText("Theme # "+clr );
        }
        if(clr < 4) {
                recreate();
                } else if (clr == 4){
                    clr = 0;
                    recreate();
                }
        }

    public void reset(View view){
        language = false;
        clr = 0;
        recreate();
    }
}
